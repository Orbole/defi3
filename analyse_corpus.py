import re
import matplotlib.pyplot as plt

longuest="" #Trouver le mot le plus long#

#########REGEX##########################
Cs = re.compile(r'(\W+$)')
Num = re.compile(r'(\d+$)')
Alpha = re.compile(r'([a-zA-Z]+$)')
#####
#Cs_Num = re.compile(r'((\W+(?=\d+)[^a-zA-Z]+)|(\d+(?=\W+)[^a-zA-Z]+))')
Cs_Num = re.compile(r'(^[^a-zA-Z]+$)')
Cs_Alpha = re.compile(r'(^\D+$)')
Num_Alpha = re.compile(r'(^[a-zA-Z0-9]+$)')
#####
Cs_Num_Alpha = re.compile(r"(.*\W(?=.*\d)(?=.*[a-zA-Z]).*)|(.*\d(?=.*\W)(?=.*[a-zA-Z]))|(.*[a-zA-Z](?=((?=.*\d)(?=.*\W))))|(.*[a-zA-Z](?=.*\W)(?=.*\d))|(.*\d(?=.*[a-zA-Z])(?=.*\W))|(.*\W(?=.*[a-zA-Z])(?=.*\d))")

#######1er caractere#####
charNum=re.compile(r'^\d.*$')
charAlpha=re.compile(r'^[a-zA-Z].*$')
charCS=re.compile(r'^[^a-zA-Z1-9].*$')

###########Compteur mots caractérisés###############################
nbr_mot_Cs = 0
nbr_mot_Num = 0
nbr_mot_Alpha = 0
nbr_mot_Cs_Num = 0
nbr_mot_Cs_Alpha = 0
nbr_mot_Num_Alpha = 0
nbr_mot_Cs_Num_Alpha = 0
###########################################


def lignes(f): #Retourne le fichier lu
	lines = fichier.readlines()
	return lines

def PremsChar(lines): #Retourne un tabelau avec le nombre de fois que le 1er caractère apparait en fonction de son type
	preumschar=[0,0,0]
	for line in lines:
		if re.match(charAlpha, line):
			#print(line)
			preumschar[0] +=1
		#########################
		elif re.match(charCS, line):
			#print(line)
			preumschar[1] +=1

		elif re.match(charNum, line):
			#print(line)
			preumschar[2] +=1
		else:
			print(line)
	return preumschar
		
def complexite(lines): # Retourne un tableau avec le nomre d'occurence du type de mots
	data=[0,0,0,0,0,0,0]
	for line in lines:
		#print(line)
		if re.match(Alpha, line):
			#print(line)
			data[0] +=1
		elif re.match(Cs, line):
			#print(line)
			data[1] +=1

		elif re.match(Num, line):
			#print(line)
			data[2] +=1
		#########################
		elif re.match(Cs_Num, line):
			#print(line)
			data[3] +=1
		elif re.match(Cs_Alpha, line):
			#print(line)
			data[4] +=1
		#########################
		elif re.match(Num_Alpha, line):
			#print(line)
			data[5] +=1
		else:
			#print(line)#Au cas ou un mot ne rencontre aucune une règle
			data[6] +=1
	return data
 
def moy_Mots(lines): #Retourne la moyenne de longueur des mots
	nb_char_tot = 0
	nb_ligne = len(lines)
	#print(nb_ligne)

	for line in lines:
		nb_char_tot += len(line)

	moy = nb_char_tot/nb_ligne
	return moy

def mot_le_plus_long(lines): #Retourne le mot le plus long ne commençant pas par un chiffre
	longuest=""
	for line in lines:
		if len(line)>len(longuest):
			if re.match(r"^\D.*", line):
				longuest=line
	return longuest

def beginNumber(lines): # Retourne les mots commençant par un chiffre
	nb_beg = 0
	
	for line in lines:
		if line[0] <= '9' and line[0] >= '0':
			nb_beg += 1
	return nb_beg
	
def endNumber(lines): #Retourne les mots finissant par un chiffre
	nb_end = 0
	
	for line in lines:
		e = len(line) - 1
		if line[e] <= '9' and line[e] >= '0':
			nb_end += 1
	return nb_end

def caracteristique(lines):
#	if#Si il a les 3 caractéristique
		for line in lines:
			if line[0] <= '9' and line[0] >= '0':
				nb_beg += 1
		#Sinon 2 caractéristique

		#Sinon1
		
fichier = open("TrainEval/train.txt", "r")
lines = lignes(fichier)
fichier.close
print(len(lines))

moy = moy_Mots(lines)
print("moy : ", moy)
print("longuest : ", mot_le_plus_long(lines))

#######Graphe complexite##########
data=complexite(lines)
print(complexite(lines))
size=[0.1,0.5,0.1,0.7,0.3,0.2,0.5]
name = ['Alpha', 'Cs', 'Num', 'CS+Num','Cs+Alpha' ,'Num+Alpha', 'CS+Num+Alpha']
plt.pie(data, labels=name, autopct='%1.1f%%', startangle=90, shadow=True, explode=size)
plt.axis('equal')
plt.show()
##################

print(PremsChar(lines))


#nb_beg = beginNumber(lines)
#print("nombre de mot debutant par un chiffre : ", nb_beg)
#pourc = nb_beg*100/len(lines)
#print("soit : ", pourc, "%")

#nb_end = endNumber(lines)
#print("nombre de mot finissant par un chiffre : ", nb_end)
#pourc = nb_end*100/len(lines)
#print("soit : ", pourc, "%")

