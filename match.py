from difflib import SequenceMatcher , get_close_matches
import re

#Ce programme permet de connaitre les mot qui ressemble à d'autre dans le corpus 
#Selon un pourcentage de ressemblance entre 0 et 1
ressemblance =0.8

#Pour des mots d'une certaine taille minimale
taille = 6
#Il peut être utiile pour une liste de mot voulu mais trop long pour l'ensemble des mots du corpus

Num = re.compile(r'(\d+$)') #  mot contenant que des chiffres
compter=0 # Pour les compter

def lignes(f): #Retourne le fichier lu
	lines = fichier.readlines()
	return lines

fichier = open("TrainEval/train.txt", "r")
lines = lignes(fichier)
fichier.close

for line in lines:
	if len(line)>taille-1 and not re.match(Num, line): # On exclue les mots d'une certaine taille et ceux contenant que des chiffres.
		sibling = get_close_matches(line, lines , n=3 , cutoff=ressemblance)
		if sibling != None and len(sibling)>1:
			compter+=1
			print(line) # On affiche le mot qui à au moins 3 ressemblance
			print(sibling) #Et ces proches jumeaux
print(compter) # Et leur nombre
